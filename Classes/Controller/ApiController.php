<?php
namespace Ausserlechner\ProjektInZahlen\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use Ausserlechner\ProjektInZahlen\Domain\Repository\ProjektInZahlenRepository;

class ApiController extends ActionController
{

    protected ProjektInZahlenRepository $projektInZahlenRepository;

    /**
     * @param ProjektInZahlenRepository $projektInZahlenRepository
     */
    public function injectSportRepository(ProjektInZahlenRepository $projektInZahlenRepository)
    {
        $this->projektInZahlenRepository = $projektInZahlenRepository;
    }

    public function apiAction()
    {

        print json_encode([
            'categories'    => $this->projektInZahlenRepository->listCategory(),
            'categoryItems' => $this->projektInZahlenRepository->listCategoryItems(),
            'currentyear'   => (int) $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['projekt_in_zahlen']['currentyear'],
        ]);
        exit();
    }



}