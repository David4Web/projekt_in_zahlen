<?php
namespace Ausserlechner\ProjektInZahlen\Controller;

use Ausserlechner\ProjektInZahlen\Domain\Repository\ProjektInZahlenRepository;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class WebAppController extends ActionController
{

    protected ProjektInZahlenRepository $projektInZahlenRepository;

    /**
     * @param ProjektInZahlenRepository $projektInZahlenRepository
     */
    public function injectProjektInZahlenRepository(ProjektInZahlenRepository $projektInZahlenRepository)
    {
        $this->projektInZahlenRepository = $projektInZahlenRepository;
    }

    public function beAction()
    {
        //DEV ::$this->view->assign('appVersion', time());
        $this->view->assign('appVersion', "3.0");
        $categoryList = $this->projektInZahlenRepository->listCategory();
        $this->view->assign('categories', json_encode($categoryList) ?: "[]");

        $categoryItems = $this->projektInZahlenRepository->listCategoryItems();
        $this->view->assign('categoryItems', json_encode($categoryItems) ?: "[]");
    }

    public function categoryAction()
    {
        $rest_json = file_get_contents("php://input");
        $objects = json_decode($rest_json);

        if($objects->action === 'add'){
            $this->projektInZahlenRepository->addCategory($objects->category);
        }elseif($objects->action === 'edit'){
            $this->projektInZahlenRepository->updateCategory($objects->category);
        }elseif($objects->action === 'delete'){
            $this->projektInZahlenRepository->updateCategory($objects->category, true);
        }

        print json_encode($this->projektInZahlenRepository->listCategory());
        exit();
    }

    public function categoryItemAction()
    {
        $rest_json = file_get_contents("php://input");
        $objects = json_decode($rest_json);

        if($objects->action === 'add'){
            $this->projektInZahlenRepository->addCategoryItem($objects->item);
        }elseif($objects->action === 'edit'){
            $this->projektInZahlenRepository->updateCategoryItems($objects->item);
        }elseif($objects->action === 'delete'){
            $this->projektInZahlenRepository->updateCategoryItems($objects->item, true);
        }

        print json_encode($this->projektInZahlenRepository->listCategoryItems());
        exit();
    }

    public function sortAction(){

        $rest_json = file_get_contents("php://input");
        $objects = json_decode($rest_json);

        $this->projektInZahlenRepository->sortList($objects->list, $objects->table);

        if($objects->table === 1){
            print json_encode($this->projektInZahlenRepository->listCategory());
        }else{
            print json_encode($this->projektInZahlenRepository->listCategoryItems());
        }

        exit();
    }
}