<?php
namespace Ausserlechner\ProjektInZahlen\Domain\Repository;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Repository;

class ProjektInZahlenRepository extends Repository
{
    private static array $DB = [
        'categoryList'    => 'tx_projekt_in_zahlen_category_list',
        'categoryItems'   => 'tx_projekt_in_zahlen_category_items',
    ];

    private static string $IMG_PATH = '/fileadmin/images/';


    public function addCategory($category){
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(self::$DB['categoryList']);
        return $queryBuilder
            ->insert(self::$DB['categoryList'])
            ->values([
                // @extensionScannerIgnoreLine
                'title'     => $category->title,
                'image'     => $category->addImage ? self::uploadImage($category->addImage) : '',
                'tstamp'    => time(),
                'crdate'    => time(),
            ])
            ->execute();
    }



    public function updateCategory($category, bool $delete=false){
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(self::$DB['categoryList']);

        if($delete){
            $queryBuilder->set('deleted', 1);
        }else{
            $queryBuilder->set('title', $category->title);
            $queryBuilder->set('image', $category->addImage ? self::uploadImage($category->addImage) : $category->image);
        }

        return $queryBuilder
            ->update(self::$DB['categoryList'])
            ->set('tstamp', time())
            ->where($queryBuilder->expr()->eq('uid', $category->uid))
            ->execute();
    }


    public function sortList($list, $table=1){

        $table = $table === 1 ? self::$DB['categoryList'] : self::$DB['categoryItems'];
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($table);

        foreach ($list as $item){
            $queryBuilder
                ->update($table)
                ->set('sort', $item->sort)
                ->where($queryBuilder->expr()->eq('uid', $item->uid))
                ->execute();
        }
    }



    public function listCategory(): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(self::$DB['categoryList']);
        $_select = ['uid', 'title', 'image', 'sort'];

        $_where = [
            $queryBuilder->expr()->eq('deleted', 0)
        ];

        return $queryBuilder
            ->select(...$_select)
            ->from(self::$DB['categoryList'])
            ->where(...$_where)
            ->orderBy('sort')
            ->execute()
            ->fetchAllAssociative();
    }



    public function addCategoryItem($item){
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(self::$DB['categoryItems']);
        return $queryBuilder
            ->insert(self::$DB['categoryItems'])
            ->values([
                'numbers'   => $item->numbers,
                'headline'  => $item->headline,
                'url'       => $item->url,
                'linktext'  => $item->linktext,
                'text'      => $item->text,
                'category'  => $item->category,
                'image'     => $item->addImage ? self::uploadImage($item->addImage) : '',
                'tstamp'    => time(),
                'crdate'    => time(),
            ])
            ->execute();
    }



    public function updateCategoryItems($item, $delete=false){
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(self::$DB['categoryItems']);

        if($delete){
            $queryBuilder->set('deleted', 1);
        }else{
            $queryBuilder->set('numbers',   $item->numbers);
            $queryBuilder->set('headline',  $item->headline);
            $queryBuilder->set('text',      $item->text);
            $queryBuilder->set('url',       $item->url);
            $queryBuilder->set('linktext',  $item->linktext);
            $queryBuilder->set('image',     $item->addImage ? self::uploadImage($item->addImage) : $item->image);
        }

        return $queryBuilder
            ->update(self::$DB['categoryItems'])
            ->set('tstamp', time())
            ->where($queryBuilder->expr()->eq('uid', $item->uid))
            ->execute();
    }



    public function listCategoryItems(){
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(self::$DB['categoryItems']);
        $_select = ['uid','numbers','headline','text','image', 'category', 'url', 'linktext', 'sort'];

        $_where = [
            $queryBuilder->expr()->eq('deleted', 0)
        ];


        $result = $queryBuilder
            ->select(...$_select)
            ->from(self::$DB['categoryItems'])
            ->where(...$_where)
            ->orderBy('sort')
            ->execute()
            ->fetchAllAssociative();

        foreach ($result as &$item){
            $isJson = strpos($item['numbers'], "}");
            $item['numbers'] = $isJson ? json_decode($item['numbers']) : $item['numbers'];
        }

        return $result;
    }



    private function uploadImage($IMAGE): string
    {

        $_dir = getcwd();
        if (TYPO3_MODE === 'BE') {
            $_dir = $_dir . '/..';
        }

        $_dir .= self::$IMG_PATH;
        $__newJpg = time().".jpg";


        if(!file_exists($_dir)){
            mkdir($_dir, 0777);
        }

        $exploded = explode(',', $IMAGE, 2);
        $encoded = $exploded[1];
        $decoded = base64_decode($encoded);

        if(file_put_contents($_dir.'/'.$__newJpg, $decoded)) {
            return $__newJpg;
        }
        return '';
    }

}