<?php
defined('TYPO3_MODE') || die();

$additionalFields = [
    'style_class' => array(
        'label' => 'Custom Class',
        'exclude' => 1,
        'config' => array(
            'type' => 'input'
        ),
    ),
];

// Add new fields to pages:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'tt_content',
    $additionalFields
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tt_content', // Table name
    '--palette--;CSS;style_class',// Field list to add
    '', // empty to add to all types
    'after:space_after_class' //position of the new settings
);


\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Ausserlechner.ProjektInZahlen',
    'Webapp',
    'Webapp'
);


// Add the new palette:
$GLOBALS['TCA']['tt_content']['palettes']['style_class'] = array(
    'showitem' => 'style_class'
);

