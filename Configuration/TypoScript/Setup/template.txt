### Template:                     **********************************************************************************

webseite = PAGE
webseite.includeCSS {
    STYLE   = {$resDir}/Public/css/style.css
    CONTENT = {$resDir}/Public/css/style_content.css
    HEADER  = {$resDir}/Public/css/style_header.css
    WEBAPP  = {$resDir}/Public/css/style_webapp.css
}
webseite.includeJSLibs {
}

projektInZahlen = FLUIDTEMPLATE
projektInZahlen {
    partialRootPath = {$resDir}/Private/Partials
    layoutRootPath = {$resDir}/Private/Layouts
    file = {$resDir}/Private/Templates/ProjektInZahlen.html

    variables {
        CONTENT < styles.content.get
        CONTENT.select.where = colPos = 1
    }
}
webseite.10 < projektInZahlen
webseite.config.contentObjectExceptionHandler = 0
webseite.shortcutIcon = {$resDir}/Public/favicon.ico