import Vue from 'vue'
import Axios from "axios";
import Vuelidate from 'vuelidate'
import Croppa from 'vue-croppa'
import 'vuetify/dist/vuetify.min.css'
import '@fortawesome/fontawesome-free/css/all.css'

/*------------ VUE --------------*/
window.myVue = Vue
window.myVue.prototype.$myBus = new Vue();

/*------------ AXIOS --------------*/
window.myVue.prototype.$axios = Axios;

/*------------ VUELIDATE --------------*/
window.myVue.use(Vuelidate);

/*------------ IMAGE CROPPA --------------*/
window.myVue.use(Croppa);

/*----------------------------------------*/
export default window.myVue
