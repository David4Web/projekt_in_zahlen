import Vue from '@BE_projekt_in_zahlen/../BE_bootstrap'
import vuetify from '../plugins/vuetify'


if(document.getElementById('adminApp')){
    new Vue({
        vuetify,
        el: '#adminApp',
        components: {
            admin_tool: () => import('@BE_projekt_in_zahlen/AdminTool'),
        }
    })
}