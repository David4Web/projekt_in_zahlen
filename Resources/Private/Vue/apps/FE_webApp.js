import Vue from '@FE_projekt_in_zahlen/../FE_bootstrap'
import Vue2TouchEvents from 'vue2-touch-events'
import isMobile from "../mixins/isMobile";

Vue.use(Vue2TouchEvents)

if(document.getElementById('webApp')){
    new Vue({
        el: '#webApp',
        mixins: [ isMobile ],
        components: {
            projekt_in_zahlen: () => import('@FE_projekt_in_zahlen/ProjektInZahlen'),
            content_element: () => import('@FE_projekt_in_zahlen/categoryDetails/categoryInNumbers'),
            content_element_mobile: () => import('@FE_projekt_in_zahlen/categoryDetails/mobileCategoryInNumbers'),
        }
    })
}