const isMobile = {
    data () {
        return {
            isMobile: false,
            mobileWindowWidth: 1000
        }
    },
    created(){
        this._mobileCheck()
        window.addEventListener('resize', this._mobileCheck);
    },
    methods: {
        _mobileCheck: function(){
            // TABLET
            let userAgent = navigator.userAgent.toLowerCase();
            const is_mobile = /(iphone|ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(userAgent);

            // DISPLAY SIZE
            let bodyWidth = document.getElementsByTagName("body")
            if(bodyWidth[0]){
                let windowWidth = bodyWidth[0].clientWidth
                this.isMobile = windowWidth < this.mobileWindowWidth || is_mobile
            }
        }
    }
}

export default isMobile
