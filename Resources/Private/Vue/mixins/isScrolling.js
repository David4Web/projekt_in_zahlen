const isScrolling = {
    data () {
        return {
            scrolling: false,
            scrollY: 0,
        }
    },
    created(){
        window.addEventListener('scroll', this._scrollCheck);
    },
    methods: {
        _scrollCheck(){
            this.scrolling = window.scrollY < this.scrollY ? false : window.scrollY > 20
            this.scrollY = window.scrollY
        }
    }
}

export default isScrolling
