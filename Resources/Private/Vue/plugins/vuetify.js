import Vue from 'vue'
import Vuetify from 'vuetify'

Vue.use(Vuetify)

export default new Vuetify({
    theme: {
        dark: false,
        themes: {
            light: {
                primary: '#a12d4e',
                //secondary: ,
                //accent: ,
                //error: ,
            },
        },
    },
    icons: {
        iconfont: 'fa',
    },
})
