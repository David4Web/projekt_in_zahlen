<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(function(){

    /********************************/
    $_VENDOR_EXT = 'Ausserlechner.ProjektInZahlen';
    $_EXTENSION = 'projekt_in_zahlen';

    /********************************/

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:'.$_EXTENSION.'/Configuration/TSconfig/Page.txt">');

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        $_VENDOR_EXT,
        'Webapp',
        [ \Ausserlechner\ProjektInZahlen\Controller\ApiController::class => 'api', ],
        [ \Ausserlechner\ProjektInZahlen\Controller\ApiController::class => 'api', ]
    );


    // VUE BUNDLE
    $PageRenderer = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Page\PageRenderer::class);
    $PageRenderer->addJsFooterLibrary("FE_VUE_BUNDLE", "/typo3conf/ext/".$_EXTENSION."/Resources/Public/webapp/webapp.bundle.js");
    $PageRenderer->addCssFile("/typo3conf/ext/".$_EXTENSION."/Resources/Public/webapp/webapp.bundle.css");
});
