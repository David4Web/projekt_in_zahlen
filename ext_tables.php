<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

call_user_func(function(){

    $_VENDOR_EXT = 'Ausserlechner.ProjektInZahlen';
    $_EXTENSION = 'projekt_in_zahlen';


    /********************************/

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        $_VENDOR_EXT,
        'tools',
        'be_projekt_in_zahlen', // (=SubmoduleName)
        'top',
        [
            \Ausserlechner\ProjektInZahlen\Controller\WebAppController::class => 'be,category,categoryItem,sort',
        ],
        [
            'access' => 'user, group',
            'icon'   => 'EXT:'.$_EXTENSION.'/Resources/Public/images/be.svg',
            'labels' => 'LLL:EXT:'.$_EXTENSION.'/Resources/Private/Language/locallang_be.xlf',
        ]
    );


});

