CREATE TABLE tt_content (
  style_class varchar(255) DEFAULT NULL
);


CREATE TABLE tx_projekt_in_zahlen_category_list (

  uid int(11) NOT NULL auto_increment,
  pid int(11) DEFAULT '0' NOT NULL,

  title varchar(255) DEFAULT '' NOT NULL,
  image varchar(255) DEFAULT '' NOT NULL,

  tstamp int(11) unsigned DEFAULT '0' NOT NULL,
  crdate int(11) unsigned DEFAULT '0' NOT NULL,
  deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
  hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
  sort  tinyint(4) unsigned DEFAULT '0' NOT NULL,

  PRIMARY KEY (uid),
  KEY parent (pid)
);


CREATE TABLE tx_projekt_in_zahlen_category_items (

  uid int(11) NOT NULL auto_increment,
  pid int(11) DEFAULT '0' NOT NULL,

  numbers   text DEFAULT '' NOT NULL,
  headline  varchar(255) DEFAULT '' NOT NULL,
  text      varchar(255) DEFAULT '' NOT NULL,
  image     varchar(255) DEFAULT '' NOT NULL,
  url       varchar(255) DEFAULT '' NULL,
  linktext  varchar(255) DEFAULT '' NULL,
  category  tinyint(4) unsigned DEFAULT '0' NOT NULL,

  tstamp int(11) unsigned DEFAULT '0' NOT NULL,
  crdate int(11) unsigned DEFAULT '0' NOT NULL,
  deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
  hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
  sort  tinyint(4) unsigned DEFAULT '0' NOT NULL,

  PRIMARY KEY (uid),
  KEY parent (pid)
);